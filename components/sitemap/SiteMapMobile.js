import React, { Component } from "react";
import PropTypes from 'prop-types'

import SiteMapSVG from "./SiteMapSVG";
import Callout from "./Callout";

import {parseAmp, matchBuldings} from  "../../lib/wp-helpers";
import sitemapHelpers from  "../../lib/sitemap-helpers";

class SiteMapMobile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      panZoom: undefined,
    }

    this.callout = React.createRef();
  }

  componentWillUnmount() {
    this.injectTenantInfo(true); //pass true arg to kill listeners
    this.injectControlListeners(true); //pass true arg to kill listeners
    this.state.panZoom.destroy();
  }

  componentDidMount() {
    this.injectTenantInfo();
    this.initMap();
    this.initPanZoom();
    this.injectControlListeners();
  }

  injectTenantInfo(isUnmount) {
    let tenants = this.props.data.tenants;

    //inject json tenant info into corresponding building in svg as data attribute
    for(let i=0; i < tenants.length; i++) {
      if(tenants[i].building) {
        let space = document.getElementById(tenants[i].building);
        if(space){
          if (isUnmount) {
            space.removeEventListener("click", this.buildingClick);
          } else {
            space.setAttribute('data-tenant', JSON.stringify(tenants[i]));
            space.addEventListener("click", this.buildingClick.bind(this));
          }
        }
      }
    }
  }

  initMap(){
    let allTenants = this.props.data.tenants;
    for(let i=0; i < allTenants.length; i++) {
      let space = document.getElementById(allTenants[i].building);

      if(space && allTenants[i].cats[0].slug){
        space.style.fill = sitemapHelpers.getCatColor(allTenants[i].cats[0].slug);
      }
    }
  }

  initPanZoom() {
    let panZoom = svgPanZoom('#sitemap-spaces', {
      zoomEnabled: false,
      dblClickZoomEnabled: false,
      controlIconsEnabled: true,
      fit: true,
      center: true,
      minZoom: .8,
      maxZoom: 3,
      customEventsHandler: sitemapHelpers.handlers,
      beforePan: this.onPan.bind(this)
    });

    this.setState({panZoom: panZoom});
  }

  injectControlListeners(isUnmount) {
    //Enable map transition animation when using controls
    let controls = document.querySelectorAll('.svg-pan-zoom-control');
    for (var i = 0; i < controls.length; i++) {
      if (isUnmount) {
        controls[i].removeEventListener("click", this.enableMapSmoothMode);
      } else {
        controls[i].addEventListener("click", this.enableMapSmoothMode);
      }
    }
  }

  enableMapSmoothMode() {
    let map = document.querySelector('.svg-pan-zoom_viewport');

    map.style.transition = 'all 400ms';
    setTimeout(function () {
      map.style.transition = 'none'; //reset to none to keep panning snappy
    }, 500); //duration of animation
  }

  buildingClick(e){
    let data = JSON.parse(e.target.getAttribute('data-tenant'));
    this.callout.current.setCalloutData(data)

    //Get Pos relative to parent
    let building = e.target.getBoundingClientRect(),
    container = document.getElementById('sitemap-spaces'),
    parent = container.getBoundingClientRect(),
    top = building.top - (parent.top),
    left = building.left - (parent.left),
    elemWidthHalfed = e.target.getBoundingClientRect().width / 2,
    width = parent.width,
    height = parent.height,
    xPos = (width/2 - left) - (elemWidthHalfed), //subtract half building width to get center
    yPos = height/2 - top;

    this.zoom({x: xPos, y: yPos}, 3, 200)
  }

  onPan(oldPan, newPan) {
    this.callout.current.hideCallout();
  }

  zoom(pos, endZoomLevel, animationTime){ // pos: {x: 1, y: 2}
    let that = this;

    let interactableArea = document.getElementById('sitemap-spaces');
    interactableArea.style.pointerEvents = "none"; //prevent interaction during animation

    if(typeof animationTime == "undefined"){
      animationTime =   300; // ms
    }

    //Generate animation steps
    let animationStepTime = 10,  // one frame per 30 ms
    animationSteps = animationTime / animationStepTime,
    animationStep = 0,
    intervalID = null,
    stepX = pos.x / animationSteps,
    stepY = pos.y / animationSteps;

    intervalID = setInterval(function(){
      if (animationStep++ < animationSteps) {
        that.state.panZoom.panBy({x: stepX, y: stepY})
      } else {
        // Cancel interval
        if(typeof endZoomLevel != "undefined"){
          let viewPort = document.querySelector('.svg-pan-zoom_viewport');
          //set transition time for smooth zooming
          viewPort.style.transition = "all " + animationTime / 400 + "s ease";
          that.state.panZoom.zoom(endZoomLevel);

          setTimeout(function(){
            viewPort.style.transition = "none";
            interactableArea.style.pointerEvents = "all"; // re-enable the pointer events after auto-panning/zooming.
          }, animationTime + 200);

          //Show callout before pointer events are re-enabled above
          setTimeout(function(){
            that.callout.current.showCallout();
          }, animationTime / 3);

        }
        clearInterval(intervalID)
      }
    }, animationStepTime)
  }

  render() {
    return (
      <div style={{position: 'relative', marginTop: 100}}>
        <div className="scroll-alert"><h4>Use two fingers to drag</h4></div>

        <Callout
          ref={this.callout}
        />

        <SiteMapSVG />
      </div>
    )
  }
}

SiteMapMobile.propTypes = {
  data: PropTypes.object
};

export default SiteMapMobile;
