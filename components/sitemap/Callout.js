import React from 'react'
import PropTypes from 'prop-types'

import {parseAmp, matchBuldings} from  "../../lib/wp-helpers";

class Callout extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      calloutImg: '',
      calloutTitle: '',
      calloutSub: '',
      isVisible: false
    }
  }

  setCalloutData(data) {
    let that = this;
    setTimeout(function () {
      let image = data.image[0] ?  data.image[0] :  "";

      that.setState({
        calloutImg: image,
        calloutTitle: parseAmp(data.title),
        calloutSub: parseAmp(data.sub),
      })
    }, 300); //wait until faded to change content
  }

  hideCallout() {
    this.setState({isVisible: false});
  }

  showCallout() {
    this.setState({isVisible: true});
  }

  render() {
    return (
      <div className={`sitemap-callout ${this.state.isVisible ? 'active' : ''}`}>
        <img id="callout-img" src={this.state.calloutImg} alt=""/>
        <h3 id="callout-title">{this.state.calloutTitle}</h3>
      </div>
    )
  }

}

export default Callout;
