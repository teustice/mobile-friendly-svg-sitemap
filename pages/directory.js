import React, { Component } from "react";
import PropTypes from 'prop-types';
import fetch from "isomorphic-unfetch";

import { Config } from "../config.js";
import PageWrapper from "../components/PageWrapper.js";
import SiteMapMobile from "../components/sitemap/SiteMapMobile.js";\

class Directory extends Component {
  static async getInitialProps(context) {
    const sitemapRes = await fetch(
      `FETCH DATA TO INJECT INTO SITEMAP HERE`
    );
    const data = await sitemapRes.json();
    return {data}
  }

  render() {
    return (
      <div className="container svg-viewport" style={{paddingTop: '150px', height: 900, position: 'relative'}}>
        <SiteMapMobile data={this.props.data.sitemap}/>
      </div>
    );
  }
}

Directory.propTypes = {
  data: PropTypes.object
};

export default PageWrapper(Directory);
