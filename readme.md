# Interactive SVG Sitemap

This prototype is a react component from a much larger application extracted for demonstration purposes. It is designed to render an SVG within an instance of [svg-pan-zoom](https://github.com/ariutta/svg-pan-zoom), and inject data into specific parts of the graphic.

One can then click parts of the svg to trigger zoom/panning animations to bring that part into focus, and render a callout containing whatever data was stored inside the polygon.

![sitemap gif](https://gitlab.com/teustice/mobile-friendly-svg-sitemap/raw/master/sitemap.gif)
