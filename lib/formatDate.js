function formatDate(date) {
  var monthNames = [
    "January", "February", "March",
    "April", "May", "June", "July",
    "August", "September", "October",
    "November", "December"
  ];

  var day = date.getDate();
  if(String(day).length == 1) {
    day = "0" + String(day);
  }
  var monthIndex = String(date.getMonth() + 1);
  if(monthIndex.length == 1) {
    monthIndex = "0" + monthIndex;
  }
  var year = date.getFullYear();
  return monthIndex + '.' + day + '.' + year;
}


function getMonthDay(date) {
  var monthNames = [
    "January", "February", "March",
    "April", "May", "June", "July",
    "August", "September", "October",
    "November", "December"
  ];

  var day = date.getDate();
  var hr = date.getHours();
  if(String(day).length == 1) {
    day = "0" + String(day);
  }
  var ampm = "AM";
  if( hr > 12 ) {
      hr -= 12;
      ampm = "PM";
  }

  var monthIndex = String(date.getMonth());
  if(monthIndex.length == 1) {
    monthIndex = "0" + monthIndex;
  }
  var year = date.getFullYear();
  return monthNames[parseInt(monthIndex)] + " " + day;
}


function getTime(date) {
  var hr = date.getHours();
  var ampm = "AM";
  if( hr > 12 ) {
      hr -= 12;
      ampm = "PM";
  }
  return hr + ampm;
}

module.exports = {
  formatDate,
  getMonthDay,
  getTime,
}
