//Event Handlers for use with svg-pan-zoom using hammer.js
// WARNING: Must contain haltEventListeners, init, destroy
let handlers = {

  haltEventListeners: ['touchstart', 'touchend', 'touchmove', 'touchleave', 'touchcancel'],

  init: function(options) {
    var instance = options.instance
    , initialScale = 1
    , pannedX = 0
    , pannedY = 0
    // Init Hammer
    // Listen only for pointer and touch events
    this.hammer = Hammer(options.svgElement, {
      inputClass: Hammer.SUPPORT_POINTER_EVENTS ? Hammer.PointerEventInput : Hammer.TouchInput
    })

    // Handle pan
    this.hammer.on('panstart panmove panend', function(ev){
      ev.preventDefault();

      document.querySelector('.scroll-alert').classList.add('opaque');
      if (ev.type === 'panend') {
        document.querySelector('.scroll-alert').classList.remove('opaque');
      }

    })

    // Enable pinch
    this.hammer.get('pinch').set({enable: true})

    // Handle pinch
    this.hammer.on('pinchstart pinchmove', function(ev){
      ev.preventDefault();

      // 2 finger panning
      if (ev.type === 'pinchstart') {
        pannedX = 0
        pannedY = 0
      }
      instance.panBy({x: ev.deltaX - pannedX, y: ev.deltaY - pannedY})
      pannedX = ev.deltaX
      pannedY = ev.deltaY
    })
  },

  destroy: function(){
    this.hammer.destroy()
  }
}

function getCatColor(catSlug, isOpaque){
  if (isOpaque) {
    switch(catSlug) {
      case 'shop':
        return 'rgba(196, 227, 222, .4)';
        break;
      case 'dine':
        return 'rgba(244, 191, 179, .4)';
        break;
      case 'live':
        return 'rgba(173, 206, 226 .4)';
        break;
      case 'soon':
        return 'rgba(195, 197, 198, .4)';
        break;
      default:
        return '#cbccce';
    }
  } else {
    switch(catSlug) {
      case 'shop':
        return '#C4E3DE';
        break;
      case 'dine':
        return '#F4BFB3';
        break;
      case 'live':
        return '#ADCEE2';
        break;
      case 'soon':
        return '#C3C5C6';
        break;
      default:
        return '#cbccce';
    }
  }
}


module.exports = {
  handlers,
  getCatColor
}
