var he = require('he'); //decodes encoded html

/////////////
// WP images
////////////

function getFeaturedImage(post, size){
  if (post.featured_media !== 0 && post._embedded['wp:featuredmedia'][0]) {
    if (post._embedded['wp:featuredmedia'][0].media_details) {
      if (post._embedded['wp:featuredmedia'][0].media_details.sizes[size]) {
        return post._embedded['wp:featuredmedia'][0].media_details.sizes[size].source_url;
      } else {
        return post._embedded['wp:featuredmedia'][0].source_url;
      }
    }
  }
}

function getEventImage(event, size){
  if (event.image !== 0 && event.image.url) {
    if (event.image.sizes) {
      if (event.image.sizes[size]) {
        return event.image.sizes[size].url;
      } else {
        return event.image.url;
      }
    }
  }
}
function getEventCarouselImage(event){
  if (event.image) {
    return event.image
  }
}

function renderBackgroundImage(tenant){
  if(tenant.acf.tenant_background_image) {
    return tenant.acf.tenant_background_image;
  } else {
    if(tenant._embedded['wp:featuredmedia']){
      return tenant._embedded['wp:featuredmedia'][0].source_url;
    }
  }
}

function isFoodtruck(event){
  if(event.isFoodTruck){
    return 'FOOD TRUCK';
  } else {
    return 'EVENT';
  }
}


///////////
// PARSERS
///////////


function parseAmp(s){
  return s.replace('&#038;', '&');
}

function decodeHtmlEntities(html) {
    var txt = document.createElement("textarea");
    txt.innerHTML = html;
    return txt.value;
}

function matchBuldings(obj, list) {
    for(let i=0; i < list.length; list++){
      if(list[i] && obj){
        if(list[i].slug == obj.slug ) {
          return true;
        }
      }
    }

    return false;
}


/////////////
// UTILITIES
/////////////
function getSnippet(text, length) {
  if(text.length > length){
    var rx = new RegExp("^.{" + length + "}[^ ]*");
    return rx.exec(text)[0] + '...';
  } else {
    return text;
  }
}

//parse html entities
function cleanPost(post) {
  post.title = he.decode(post.title);
  post.content = he.decode(post.content);
  return post;
}

function prepSearch(text) {
  return text.toString().toLowerCase()
    .replace(/\s+/g, '+')           // Replace spaces with -
    .replace(/[^\w\-]+/g, '')       // Remove all non-word chars
    .replace(/\-\-+/g, '+')         // Replace multiple - with single -
    .replace(/^-+/, '')             // Trim - from start of text
    .replace(/-+$/, '');            // Trim - from end of text
}

function trackPageView(url) {
  try {
    window.gtag('config', 'UA-69512535-1', {
      page_location: url
    });
  } catch (error) {
    // silences the error in dev mode
    // and/or if gtag fails to load
  }
}


module.exports = {
  getFeaturedImage,
  getEventImage,
  getEventCarouselImage,
  renderBackgroundImage,
  parseAmp,
  matchBuldings,
  getSnippet,
  decodeHtmlEntities,
  cleanPost,
  prepSearch,
  isFoodtruck,
  trackPageView
}
